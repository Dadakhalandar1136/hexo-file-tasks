# **Truthy and Falsy**
<br><br>
Truthy and Falsy are the concepts of [JavaScript](https://developer.mozilla.org/en-US/docs/Glossary/JavaScript) where it will be used to work on the [conditional statements](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/if...else).
<br><br>

## **Truthy (True)**
[Truthy](https://developer.mozilla.org/en-US/docs/Glossary/Truthy) (is aslo called as true) value is acutally used to check the given condition is **true** or not. It will be used by declaring the [Boolean](https://developer.mozilla.org/en-US/docs/Glossary/Boolean) value. When we will use **true** condition then all values will be truthy expect when you'll declare as [falsy](https://developer.mozilla.org/en-US/docs/Glossary/Falsy).
<br><br>
**Example for Truthy Values are :**
<br><br>
```
if (true)
if ({})
if ("false")
if (new Date())
if ([])
if (42)
if (-42)
if (12n)
if (Infinity)
if (-Infinity)
if ("0")
```
<br><br>
**Example Codes :**

    if (true) {
        console.log("Truthy condition"); 
        // prints Truthy condition
    }

    if ({}) {
        console.log(true); // Prints true
    }
<br>
As shown about the if statement will works as per the condition given.
<br><br>

## **Falsy (false)**
The Falsy value is used for to check the given condition is false or not. Whenever we declare the [falsy](https://developer.mozilla.org/en-US/docs/Glossary/Falsy) value in [Boolean](https://developer.mozilla.org/en-US/docs/Glossary/Boolean) considered as false.
<br>
Falsy values are used for [conditional](https://developer.mozilla.org/en-US/docs/Glossary/Conditional) statements.
<br><br>
**Example for Falsy values :**

    false       // false keyword.

    0           // zero Number.

    -0          // Negative zero

    null        // using null declaration

    undefined   // Declaring undefined 

    NaN         // declaring NaN(Not a Number)

    0n          // BigInt zero

    "", '', ``  // declaring the operators.
<br><br>
**Example code :**
<br>

    if (array === false) {
        console.log(array); //returns false
    }

    if (null) {
        // it will not reachable.
    }
<br><br>
**Example for Truty and Falsy**
<br>
    
    if (array) {
        console.log("Truthy");
    }

    if (array === false) {
        console.log("Falsy");
    }